// CRUD Operations

// Inserting Documents (Create)
// Syntax: db.collectionName.insertOne({object});
// Javascript: object.object.method({object});
db.users.insertOne({
        "firstName": "Jane",
        "lastName": "Doe",
        "age": 21,
        "contact": {
                "phone": "09196543210",
                "email": "janedoe@gmail.com"
            },
            
        "courses": ["CSS", "Javascript", "Phyton"],
        "department": "none"    
    });

// Insert Many
// Syntax: db.collectionName.insertMany([{objectA},{objectB}]);
db.users.insertMany([{
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "contact": {
                "phone": "09179876543",
                "email": "stephenhawking@gmail.com"
            },
            
        "courses": ["React", "PHP", "Phyton"],
        "department": "none"    
},
{
         "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "contact": {
                "phone": "09061234567",
                "email": "neilarmstrong@gmail.com"
            },
            
        "courses": ["React", "Laravel", "SASS"],
        "department": "none" 
}   
]);

// Finding documents (Read) Operation
/* Find Syntax:
	db.collectionName.find();
	db.collectionName.find({field: value});
*/
db.collectionName.find();
db.users.find({"lastName": "Doe"});
db.users.find({"lastName": "Doe","age": 25}).pretty();
